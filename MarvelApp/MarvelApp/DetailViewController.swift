//
//  DetailViewController.swift
//  MarvelApp
//
//  Created by Jaime Yerovi on 3/1/18.
//  Copyright © 2018 jy. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var pokemon:Pokemon?
    
    @IBOutlet weak var pokemonImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.title = pokemon?.name
        
        let pkService = PokemonService()
        
        pkService.getPokemonImage(id:(pokemon?.id)!) { (pkImage) in
        
            self.pokemonImageView.image = pkImage
            
        
        }
        
    }

   

}
