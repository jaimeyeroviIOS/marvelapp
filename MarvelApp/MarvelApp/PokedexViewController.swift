//
//  PokedexViewController.swift
//  MarvelApp
//
//  Created by Jaime Yerovi on 29/11/17.
//  Copyright © 2017 jy. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate {

    var pokemonArray:[Pokemon] = []
    var pokemonIndex = 0
    
    @IBOutlet weak var pokedexTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let service = PokemonService()
        service.delegate = self
        service.downloadPokemons()
    }

    //MARK:- TableView

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func get20FistPokemons(pokemons: [Pokemon]) {
        pokemonArray = pokemons
        pokedexTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section {
//        case 0:
//            return 5
//        default:
//            return 10
//        }
        return pokemonArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewTableViewCell
        
        
        cell.fillData(pokemon: pokemonArray[indexPath.row])
        //cell.textLabel?.text = pokemonArray[indexPath.row].name    sin celda creada 
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Sección 1"
        default:
            return "Sección 2"
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonIndex = indexPath.row
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemonDetail = segue.destination as! DetailViewController
        
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
        
    }
    
    
}
